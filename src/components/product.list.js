import { useDispatch, useSelector } from "react-redux";
import { useEffect, useState } from "react";
import {GetItems, AdjustOutput, SetCart, SetStock } from "../redux/actions/productActions";
import  styles from "./productlist.module.css";
import Cart from "./Cart";

export default function ProductList() {
    const dispatch = useDispatch();
    const {items, cart, loading, error} = useSelector((state) => state); 

    useEffect(() => {
        dispatch(GetItems());
    }, []);

    useEffect(() => {
        dispatch(SetCart())
    }, [items]);

    const handleOutput = (uid, value, stock) => {
      dispatch(AdjustOutput({uid, value}));
      dispatch(SetStock({uid, stock}));
    }
    
    return (
        /*  PENDING > LOADING TRUE
            SUCCES > ITEMS FALSE
            FAILED > ERRROR FALSE
        */
        <>
        <div className={styles.grid}>
            {(loading && !error) ? <div className='loading'>Loading....</div> :
            items.map((item) => (
                <div className={styles.product} key={item.uid}>
                    <div className={styles.product_image}><img src={item.image.url} alt={item.image.altText} /> </div>
                    <div className={styles.product_name}>{item.productName}</div>
                    <div className={styles.product_price}>{`Rp.${parseInt(item.price).toLocaleString()},-`}</div>
                    <div className={styles.product_quantity}>{`Stock: ${item.stock}`}</div>
                    <div className={styles.input_container}>
                        <button 
                            disabled={item.output <= 0 ? true : false}
                            onClick={() => handleOutput(item.uid, item.output - 1, item.stock + 1)}>
                        -</button> 
                        <input 
                            min='0'
                            className={styles.input} 
                            value={item.output}
                            onChange={(e) => handleOutput(item.uid, e.target.value, item.stock - e.target.value)}/>
                        <button
                            disabled={item.output >= item.availableQuantity ? true : false} 
                            onClick={() => handleOutput(item.uid, item.output + 1, item.stock - 1)} >+</button>
                    </div>
                </div>
                )) 
            } 
            {error && <div>Unexpeccted Error Occured </div>}
        </div>
            {cart.length ? (
                //fixed botom, kasih height
                <Cart
                button='Procces'
                link='/cart'
                total={ cart.length ?
                    cart.map((object) => object.output * object.price).reduce((a, b) => a + b)
                    : 0
                    }
                />
            ) : null}
        </>
    )
}