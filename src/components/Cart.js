import { Link} from "react-router-dom";
import styles from'./Cart.module.css';

const Cart = (props) => {
    return (
        <div className={styles.cart_container}>
            <nav>
                <div className={styles.container_navbot}>
                        <div>
                            {`Jumlah Belanja : Rp. ${parseInt(props.total).toLocaleString()},-`}
                        </div>
                    <Link to={props.link} className={styles.button_navbot}>
                        <button>
                            {props.button}
                        </button>
                    </Link>
                </div>
            </nav>
        </div>
    );
}
export default Cart;