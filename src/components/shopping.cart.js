import { useSelector } from "react-redux";
import styles from './ShoppingCart.module.css';
import Cart from "./Cart";

export default function ShoppingCart() {
    const cart = useSelector((state) => state);
    console.log('cart: ', cart);

    return (
       <>
            <Cart 
                button='Payment'
                link='/'
                total={cart.map((object) => object.output * object.price).reduce((a, b) => a + b)}
            />
        </>
    )
}