import styles from "./header.module.css"

export default function Header() {
    return (
        <>
            <a target="_blank" href="" className={styles.header}>PowerIO Shop
            </a>
                <hr/>
        </>
    )
}