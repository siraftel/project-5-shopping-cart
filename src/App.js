import './App.css';
import { BrowserRouter as Router, Routes, Route } from 'react-router-dom';
import Header from './components/header';
import ProductList from './components/product.list';
import ShoppingCart from './components/shopping.cart';
import Error from './pages/error'


function App() {
  return (
    <div >
      <Header/>
      <div>
        <Routes>
          <Route  exact path="/" element={<ProductList/>} />
          <Route path="/cart" element={<ShoppingCart/>} />
          <Route path="*" element={<Error/>} /> 
        </Routes>
      </div>
    </div>
  );
}

export default App;
