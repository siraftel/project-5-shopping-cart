
const initialState = {
    items: [],
    cart: [],
    loading: false,
    error: ""
}

export default function productReducer(state = initialState, action) {
    const {type, payload} = action;
    
    switch(type) {
        case 'ITEM_GET_PENDING':
            return {
                ...state,
                loading: true
            }
        case 'ITEM_GET_SUCCES':
            return {
                ...state,
                loading: false,
                items: action.payload
            }
        case 'ITEM_GET_FAILED': 
            return {
                ...state,
                loading: false,
                error: action.payload
            }
        case 'SET_CART':
            return {
                ...state,
                cart: state.items.filter((object) => object.output > 0)
            }
        case 'ADJUST_OUTPUT':
            return {
                ...state,
               items: state.items.map((item) => {
                   if(item.uid === payload.uid) {
                       return { 
                           ...item,
                           output: payload.value
                       }
                    }
                    else {
                        return item;
                    }
               })
            }
        case 'SET_STOCK':
            return {
                ...state,
                items: state.items.map((item) => {
                    if(item.uid === payload.uid) {
                        return {
                            ...item,
                            stock: payload.stock,
                        }
                    }
                    else {
                        return item
                    }
                })
            }
        default:
        return state;
    }
};