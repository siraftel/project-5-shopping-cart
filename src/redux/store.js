import { createStore, applyMiddleware, compose, /*combineReducers */ } from 'redux';
import thunk from 'redux-thunk';
import productReducer from './reducers/product.reducer';


const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

// const rootReducer = combineReducers({
//     productReducer,
    
// })

const store = createStore(productReducer, composeEnhancers(applyMiddleware(thunk)));
//const store = createStore(counterReducer, composeEnhancers(applyMiddleware(thunk)));


export default store;