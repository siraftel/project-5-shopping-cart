import axios from "axios"

const GetItems = () => {
    return(dispatch, getState) => {
        dispatch({type: 'ITEM_GET_PENDING'});
        //API call
        axios.get('https://my-json-server.typicode.com/irhamhqm/placeholder-shops/items') 
            .then((response) => {
                const productData = response.data.map(item => ({...item, output: 0, stock: 100}));
                dispatch({type: 'ITEM_GET_SUCCES', payload: productData});
            })
            .catch((error) => {
                dispatch({type:'ITEM_GET_FAILED', payload: error });
            });
    }
}
export const SetCart = (data) => {
    return {
       type: 'SET_CART',
       payload: data
    }
}
export const AdjustOutput = (data) => {
    return {
        type: 'ADJUST_OUTPUT',
        payload: data
    }
     
}
export const SetStock = (data) => {
    return {
        type: 'SET_STOCK',
        payload: data
    }
       
}


export {
    GetItems
};

